import requests
import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv
load_dotenv()
downloadPath = os.getenv('downloadPath')
loginUrl = 'https://app.clickup.com/login'
domainUrl = 'https://app.clickup.com/3696740/docs'

def checkIfFileIsDownloaded(currentFiles):
	length = len(os.listdir(downloadPath))
	print('current: ', currentFiles, 'download: ',length)
	if(length > currentFiles):
		return True
	elif(length <= currentFiles):
		return False

def main():
	
	prefs = {"download.default_directory": downloadPath}
	chrome_options = Options()
	chrome_options.add_argument("--kiosk")
	chrome_options.add_experimental_option("prefs", prefs);
	driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
	driver.get(f'{loginUrl}')
	
	time.sleep(1)

	# login clickup
	try:
		email = driver.find_element(
			By.ID, 
			'login-email-input'
		)
		email.send_keys(os.getenv('email'))
		print('enter email success')
	except:
		print('enter email fail')

	try: 
		password = driver.find_element(
			By.ID, 
			'login-password-input'
		)
		password.send_keys(os.getenv('password'))
		print('enter password success')
	except:
		print('enter email fail')

	login = driver.find_element(
		By.XPATH, 
		value="//button[@type='submit']"
	)
	login.click()
	time.sleep(3)

	# redirect to docs
	driver.get(domainUrl)
	time.sleep(5)
	# scrollToSharing = driver.find_element(
	# 	By.XPATH, 
	# 	value="//div[contains(@class, 'cu-doc-list__body-column cu-doc-list__body-column_share')]"
	# )
	# driver.execute_script("arguments[0].scrollIntoView(true);", scrollToSharing);
	# time.sleep(3)
	
	allfolders = driver.find_elements(
		By.XPATH, 
		value="//div[@class='cu-doc-list-row-item__share-icon icon ng-star-inserted']/*[name()='svg'][@class='ng-star-inserted']"
	)
	for f in allfolders:
		print(f)
		f.click()
		time.sleep(3)

		export = driver.find_element(
			By.XPATH,
			value="//div[contains(@class, 'cu-privacy-settings__summary-btn')]/div[text() = ' Export ']"
		)
		export.click()
		time.sleep(1)
		
		currentFiles = len(os.listdir(downloadPath))
		pdfDownload = driver.find_element(
			By.XPATH,
			value="//div[contains(@class, 'cu-doc-export__option-text cu-doc-export__option-text_caps') and text()=' PDF ']"
		)
		pdfDownload.click()
		time.sleep(10)
		
		
		while not checkIfFileIsDownloaded(currentFiles):
			print('file is downloading')
			time.sleep(10)

		export.click()
		time.sleep(1)

		currentFiles = len(os.listdir(downloadPath))
		htmlDownload = driver.find_element(
			By.XPATH,
			value="//div[contains(@class, 'cu-doc-export__option-text cu-doc-export__option-text_caps') and text()=' HTML ']"
		)
		htmlDownload.click()
		time.sleep(10)

		while not checkIfFileIsDownloaded(currentFiles):
			print('file is downloading')
			time.sleep(10)

		close = driver.find_element(
			By.XPATH,
			value="//div[contains(@data-test, 'share-entity__close')]"
		)
		close.click()
		time.sleep(1)
		continue
		
	print('finish download')
	time.sleep(100)
if __name__ == "__main__":
	main()