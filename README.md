# Clickup backup
## Summary
Chelpis Clickup workspace will be downgraded in 7/1 2022, our goal is to develop a script to automaticcally store the attachments and docs.
## Plan 
- stages of backup plan
  - first stage:
  download attachments and docs to local machine(as a file buffer)
  - second stage(stopped):
  upload attachments and docs to google drive and identify attachments by task id
## How to use
- clone this project to your local machine
```shell=
$ git clone https://gitlab.com/dorian.liu/clickup-backup.git
```
- install dependencies
```shell=
$ pip3 install pandas
$ pip3 install beautifulsoup4
```
- backup attachments
  - run backupAttachments.py script:
  ```shell=
  $ python3 backupAttachments.py
  ```
  - all your directories and files will be written to "attachmentFiles"
- backup docs
	- fill up env.example & rename it to .env
  - run backupDocs.py script:
  ```shell=
  $ python3 backupDocs.py
  ```
  - all your directories and files will be written to "setupPath"