import pandas as pd
import requests
import os
import ast
import re

# Validate UUID
uuid_pattern = '[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}'

def main():
	# read csv
	df = pd.read_csv('csv/attachments_fulldata.csv')
	tasks = df[['Task ID', 'Attachments']]

	# read array of objects in column['Attachments']
	tasksAttachments = tasks['Attachments'].apply(ast.literal_eval)

	# batch write file
	print('file backup start...')
	for index in range(len(tasks.Attachments)):
		# print(tasks.loc[index,'Attachments'])
		
		if(len(tasksAttachments[index]) != 0):
			for objIndex in range(len(tasksAttachments[index])):
				# print(tasksAttachments[index][objIndex])
				response = requests.get(tasksAttachments[index][objIndex]['url'])
				
				uuid = re.search(uuid_pattern, tasksAttachments[index][objIndex]['url'])
				# print(tasksAttachments[index][objIndex]['url'])
				# print(uuid.group(0))
				filePath = 'attachmentFiles/%s' % (tasks.loc[index,'Task ID'])
				fileName = str(uuid.group(0)) + '-' + tasksAttachments[index][objIndex]['title']
				
				os.makedirs(filePath, exist_ok=True)
				with open(os.path.join(filePath, fileName), "wb") as file:
					file.write(response.content)
	print('file backup finished!')
 
if __name__ == '__main__':
	main()